﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using OAuth2.App_Start;

namespace OAuth2.Controllers
{
    public class LoginController : ApiController
    {
        public IEnumerable<string> Get()
        {
            return new string[] { "hello" };
        }

        public ModelUserList Post([FromBody]ModelLogin lm)
        {
            SqlConnection conn = new DbConController().conn;

            conn.Open();
            SqlDataReader reader = null;
            SqlCommand cmd = new SqlCommand("SELECT * FROM UserList WHERE UserId = @UserId AND PassWord = @PassWord", conn);
            cmd.Parameters.AddWithValue("@UserId", lm.UserID);
            cmd.Parameters.AddWithValue("@PassWord", lm.UserPwd);
            reader = cmd.ExecuteReader();

            ModelUserList mu = new ModelUserList();
            while (reader.Read())
            {
                mu.id = reader["id"].ToString();
                mu.UserName = reader["UserName"].ToString();
                mu.UserID = reader["UserId"].ToString();
                mu.UserPwd = reader["PassWord"].ToString();
                mu.Sex = reader["Sex"].ToString();
            }
            return mu;
        }
    }
}
