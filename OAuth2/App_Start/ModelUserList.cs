﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAuth2.App_Start
{
    public class ModelUserList
    {
        public string id { set; get; }
        public string UserName { set; get; }
        public string UserID { set; get; }
        public string UserPwd { set; get; }
        public string Sex { set; get; }
    }
}